﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WatiN.Core;
[assembly: RequiresThread]

namespace SR.Web.Test.UI
{
    class FlowersDeliveryPageTest
    {
        [Test, RequiresThread]
        public void AddItems_ShouldReturn_MoreItems()
        {
            using (var browser = new IE("http://srbuild.cloudapp.net/flowers-delivery-amvrosievka/flowers/roses/"))
            {
                var countInitial = browser.Elements.Count(element => string.Compare(element.ClassName, "new_prod_box", true) == 0);
                browser.Button(Find.ById("btnShowMore")).Click();
                var countFinal = browser.Elements.Count(element => string.Compare(element.ClassName, "new_prod_box", true) == 0);

                Assert.IsTrue(countFinal > countInitial);
            }
        }
    }
}
